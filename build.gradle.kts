val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val kotlinVersion: String by project
val ktorVersion: String by project
val mcs2kVersion: String by project
plugins { id("me.champeau.gradle.jmh"); kotlin("jvm") }
group = "io.github.commandertvis"

repositories {
    jcenter()
    maven(url = "https://gitlab.com/api/v4/projects/11000558/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/17803458/packages/maven")
}

dependencies {
    implementation("io.github.commandertvis:mcs2k:$mcs2kVersion")
    implementation(kotlin("stdlib-jdk8"))
}

jmh { fork = 1 }

tasks.compileJmhKotlin.get().kotlinOptions {
    apiVersion = kotlinApiVersion
    jvmTarget = kotlinJvmTarget
    languageVersion = kotlinLanguageVersion
}
