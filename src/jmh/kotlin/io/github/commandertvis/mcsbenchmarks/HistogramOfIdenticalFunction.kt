package io.github.commandertvis.mcsbenchmarks

import io.github.commandertvis.mcs.McsInterpreter
import org.openjdk.jmh.annotations.*

@BenchmarkMode(Mode.All)
@Warmup(iterations = 0)
@Measurement(iterations = 10)
open class HistogramOfIdenticalFunction {
    @Benchmark
    fun oneWorker() {
        McsInterpreter(workersQuantity = 1).use {
            it.histogram(
                expression = "x",
                nValues = 1000000L,
                xMin = 0.0,
                xMax = 1.0,
                histogramMin = 0.0,
                histogramMax = 1.0,
                nBins = 10L
            )
        }
    }

    @Benchmark
    fun fourWorkers() {
        McsInterpreter(workersQuantity = 4).use {
            it.histogram(
                expression = "x",
                nValues = 1000000L,
                xMin = 0.0,
                xMax = 1.0,
                histogramMin = 0.0,
                histogramMax = 1.0,
                nBins = 10L
            )
        }
    }
}
